package pdfsplit

import (
	"encoding/csv"
	"io"
	"os"
	"path/filepath"
	"strconv"
	s "strings"

	pdf "github.com/pdfcpu/pdfcpu/pkg/api"
	pdfCore "github.com/pdfcpu/pdfcpu/pkg/pdfcpu"
)

// Split takes a PDF, a CSV and and output dir, and outputs the sub-PDF files
func Split(pdfPath string, csvPath string, outputPath string) error {
	csvFile, err := os.Open(csvPath)
	if err != nil {
		return err
	}

	csvReader := csv.NewReader(csvFile)

	inputContext, err := pdf.ReadContextFile(pdfPath)
	if err != nil {
		return err
	}

	for {
		record, err := csvReader.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			return err
		}

		startPage, err := strconv.Atoi(record[1])
		if err != nil {
			return err
		}
		endPage, err := strconv.Atoi(record[2])
		if err != nil {
			return err
		}

		selectedPages := make([]int, endPage-startPage+1)

		for i := 0; i <= endPage-startPage; i++ {
			selectedPages[i] = startPage + i
		}

		ctxDest, err := pdfCore.CollectPages(inputContext, selectedPages)
		if err != nil {
			return err
		}

		songName := record[0]
		songName = s.ReplaceAll(songName, "/", "_")
		songName = s.ReplaceAll(songName, "\\", "_")

		fWrite, err := os.Create(filepath.Join(outputPath, songName) + ".pdf")
		if err != nil {
			return err
		}

		defer fWrite.Close()

		err = pdf.WriteContext(ctxDest, fWrite)
		if err != nil {
			return err
		}
	}

	return nil
}
