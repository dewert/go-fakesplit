# Fakesplit: a splitter for fakebooks

This is a small utility to split a jazz fakebook into individual PDFs. It should be considered
pre-alpha quality, and while I won't get mad, it's probably not worth your time to report bugs.
Any tips about my amateur Go code would be appreciated.

## Usage

- Run the executable
- Supply the fakebook PDF, the CSV index, and the output folder
- Hit "Split" and wait for the Success message
- Enjoy your individual songs as PDF files!

## Notes

- The CSV index must consist of three columns: song title, start page, and end page. Commonly
  available CSV indices may not have end pages. I intend to handle this in the application when I
  clean it up
- The output folder must exist

## Libraries

- gotk3 (https://github.com/gotk3/gotk3)
- gtk3 (https://gitlab.gnome.org/GNOME/gtk)
- pdfcpu (https://github.com/pdfcpu/pdfcpu)