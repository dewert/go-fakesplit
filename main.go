package main

import (
	"errors"
	"log"
	"os"

	"github.com/gotk3/gotk3/glib"
	"github.com/gotk3/gotk3/gtk"

	"fakesplit/pdfsplit"
)

const appID = "ca.dvan.fakesplit"

func main() {
	// Create a new application.
	application, err := gtk.ApplicationNew(appID, glib.APPLICATION_FLAGS_NONE)
	errorCheck(err)

	// Connect function to application activate event
	application.Connect("activate", func() {

		// Get the GtkBuilder UI definition in the glade file.
		builder, err := gtk.BuilderNewFromFile("ui/main.glade")
		errorCheck(err)

		// Get the object with the id of "main_window".
		obj, err := builder.GetObject("main_window")
		errorCheck(err)

		// Verify that the object is a pointer to a gtk.ApplicationWindow.
		win, err := isWindow(obj)
		errorCheck(err)

		// Hook up the quit button
		quitButtonObj, _ := builder.GetObject("quitButton")
		quitButton := quitButtonObj.(*gtk.Button)
		quitButton.Connect("clicked", func() {
			application.Quit()
		})

		// Hook up the browse buttons
		pdfBrowse, pdfEntry := getBrowseAndEntry(builder, "pdf")
		pdfBrowse.Connect("clicked", makeBrowseHandler("Select Fakebook PDF", win, "application/pdf", pdfEntry))

		csvBrowse, csvEntry := getBrowseAndEntry(builder, "csv")
		csvBrowse.Connect("clicked", makeBrowseHandler("Select Index CSV", win, "text/csv", csvEntry))

		outputBrowse, outputEntry := getBrowseAndEntry(builder, "output")
		outputBrowse.Connect("clicked", makeFolderBrowseHandler("Select Output Folder", win, outputEntry))

		statusBarObj, err := builder.GetObject("statusbar")
		errorCheck(err)

		statusBar := statusBarObj.(*gtk.Statusbar)
		statusBar.Push(42, "Select files for splitting...")

		splitButtonObj, err := builder.GetObject("splitButton")
		errorCheck(err)

		splitButton := splitButtonObj.(*gtk.Button)
		splitButton.Connect("clicked", func() {
			statusBar.Push(42, "Splitting PDF...")

			pdfPath, _ := pdfEntry.GetText()
			csvPath, _ := csvEntry.GetText()
			outputPath, _ := outputEntry.GetText()

			err = pdfsplit.Split(pdfPath, csvPath, outputPath)
			if err != nil {
				statusBar.Push(42, err.Error())
			}
			statusBar.Push(42, "Success!")
		})

		// Show the Window and all of its components.
		win.Show()
		application.AddWindow(win)
	})

	// Launch the application
	os.Exit(application.Run(os.Args))
}

func isWindow(obj glib.IObject) (*gtk.Window, error) {
	// Make type assertion (as per gtk.go).
	if win, ok := obj.(*gtk.Window); ok {
		return win, nil
	}
	return nil, errors.New("not a *gtk.Window")
}

func errorCheck(e error) {
	if e != nil {
		// panic for any errors.
		log.Panic(e)
	}
}

func makeBrowseHandler(title string, window *gtk.Window, mime string, destination *gtk.Entry) func() {
	return func() {
		filePicker, err := gtk.FileChooserNativeDialogNew(title, window, gtk.FILE_CHOOSER_ACTION_OPEN, "Select", "Cancel")
		errorCheck(err)
		filter, err := gtk.FileFilterNew()
		errorCheck(err)
		filter.AddMimeType(mime)
		filePicker.SetFilter(filter)
		response := filePicker.Run()
		if response == int(gtk.RESPONSE_ACCEPT) {
			destination.SetText(filePicker.GetFilename())
		}
	}
}

func makeFolderBrowseHandler(title string, window *gtk.Window, destination *gtk.Entry) func() {
	return func() {
		folderPicker, err := gtk.FileChooserNativeDialogNew(title, window, gtk.FILE_CHOOSER_ACTION_SELECT_FOLDER, "Select", "Cancel")
		errorCheck(err)
		response := folderPicker.Run()
		if response == int(gtk.RESPONSE_ACCEPT) {
			destination.SetText(folderPicker.GetFilename())
		}
	}
}

func getBrowseAndEntry(builder *gtk.Builder, identifier string) (*gtk.Button, *gtk.Entry) {
	buttonObj, _ := builder.GetObject(identifier + "Browse")
	button := buttonObj.(*gtk.Button)
	entryObj, _ := builder.GetObject(identifier + "Entry")
	entry, _ := entryObj.(*gtk.Entry)
	return button, entry
}
