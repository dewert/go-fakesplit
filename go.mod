module fakesplit

go 1.14

require (
	github.com/gotk3/gotk3 v0.4.0
	github.com/pdfcpu/pdfcpu v0.3.4
)
